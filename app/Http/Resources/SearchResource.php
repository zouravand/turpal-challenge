<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $name
 * @property mixed $thumbnail
 */
class SearchResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->name,
            'minimumPrice' => $this->minimumPrice ?? null,
            'thumbnail' => $this->thumbnail,
        ];
    }
}
