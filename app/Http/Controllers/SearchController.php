<?php

namespace App\Http\Controllers;

use App\Http\Resources\SearchCollection;
use App\Models\Product;
use App\Services\ContractorService\Contractor;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request): SearchCollection
    {
        $queryToSearch = $request->get('query', '');

        $period = CarbonPeriod::create(
            $request->get('startDate') ?? now()->startOfDay(),
            $request->get('endDate') ?? now()->addWeeks(2)->endOfDay(),
        );

        foreach (config('contractor.available', []) as $contractorsSlug) {
            $contractor = Contractor::type($contractorsSlug);

            $period->forEach(function (Carbon $day) use ($contractor) {
                $contractor->getAvailableProducts($day);
            });
        }

        $products = Product::query()
            ->whereHas('availabilities', function ($query) use ($request) {
                $query->available($request->get('startDate') ?? null, $request->get('endDate') ?? null,);
            })
            ->where('name', 'LIKE', '%' . $queryToSearch . '%')
            ->get();

        return new SearchCollection($products);
    }
}
