<?php

namespace App\Jobs;

use App\Models\Product;
use App\Services\ContractorService\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateProductDetails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Contractor $contractor, private array $product)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $productDetails = $this->contractor->getProductDetail($this->product['code']);

        if ($selectedProduct = Product::query()
            ->where('code', $this->product['code'])
            ->where('contractor', $this->product['contractor'])
            ->first()) {
            /** @var Product $selectedProduct */
            $selectedProduct->description = $productDetails['description'];
            $selectedProduct->thumbnail = $productDetails['thumbnail'];
            $selectedProduct->save();
        }
    }
}
