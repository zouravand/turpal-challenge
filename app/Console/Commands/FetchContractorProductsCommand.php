<?php

namespace App\Console\Commands;

use App\Jobs\UpdateProductDetails;
use App\Services\ContractorService\Contractor;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class FetchContractorProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Third Party Products';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {

        foreach (config('contractor.available', []) as $contractorsSlug) {
            $contractor = Contractor::type($contractorsSlug);

            $this->info('Fetching data from ' . $contractor->slug);

            $products = $contractor->getProductList();
            $contractor->storeProductList($products);
            foreach ($products as $product)
            {
                UpdateProductDetails::dispatch($contractor, $product);
            }
        }

        return CommandAlias::SUCCESS;
    }
}
