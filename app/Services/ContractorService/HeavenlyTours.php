<?php

namespace App\Services\ContractorService;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class HeavenlyTours extends Contractor
{
    public string $slug = 'heavenly-tours';

    function getProductList(): array
    {
        $response = Http::baseUrl(config('services.heavenly-tours.base-url'))
            ->timeout(15)
            ->retry(3)
            ->get('/tours');

        if ($response->ok())
            return $this->productsMapper($response->json());

        return [];
    }

    function storeProductList(array $products): int
    {
        return Product::query()
            ->upsert($products, ['code', 'contractor']);
    }

    function getProductDetail(string $code): ?array
    {
        $response = Http::baseUrl(config('services.heavenly-tours.base-url'))
            ->timeout(15)
            ->retry(3)
            ->get('/tours/' . $code);

        if ($response->ok())
            return $this->productDetailMapper($response->json());

        return [];
    }

    function getProductAvailability(string $code, Carbon $travelDate): bool
    {
        $response = Http::baseUrl(config('services.heavenly-tours.base-url'))
            ->timeout(15)
            ->retry(3)
            ->get('/tours/' . $code . '/availability?travelDate=' . $travelDate->format('Y-m-d'));

        if ($response->ok())
            return $response->json('available');

        return false;
    }

    function getAvailableProducts(Carbon $travelDate): void
    {
        $products = $this->getAvailableProductsPrices($travelDate);

        foreach ($products as $product)
        {
            if($this->getProductAvailability($product['tourId'],$travelDate))
            {
                if($selectedProduct = Product::query()
                    ->where('code', $product['tourId'])
                    ->where('contractor',$this->slug)
                    ->first())
                {
                    /** @var Product $selectedProduct */
                    $selectedProduct->availabilities()->updateOrCreate([
                        'price' => $product['price'],
                        'start_time' => $travelDate,
                        'end_time' => $travelDate,
                    ]);
                }
            }
        }
    }

    function getAvailableProductsPrices(Carbon $travelDate): array
    {
        $response = Http::baseUrl(config('services.heavenly-tours.base-url'))
            ->timeout(15)
            ->retry(3)
            ->get('/tour-prices?travelDate=' . $travelDate->format('Y-m-d'));

        if ($response->ok())
            return $response->json();

        return [];
    }


    private function productDetailMapper(array $product): array
    {
        $thumbnail = Arr::where($product['photos'], function ($photo) {
            return $photo['type'] == 'thumbnail';
        });

        return [
            'code' => $product['id'],
            'name' => $product['title'],
            'description' => $product['description'],
            'thumbnail' => Arr::first($thumbnail) ?? null,
            'contractor' => $this->slug
        ];
    }

    private function productsMapper(array $products): array
    {
        return Arr::map($products, function ($product) {
            return [
                'code' => $product['id'],
                'name' => $product['title'],
                'description' => $product['excerpt'],
                'contractor' => $this->slug
            ];
        });
    }
}
