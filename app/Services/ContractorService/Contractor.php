<?php
namespace App\Services\ContractorService;

use Carbon\Carbon;
use Illuminate\Support\Str;

abstract class Contractor {
    public string $slug = '';

    static private array $contractors = [];
    static function type(string $slug): Contractor
    {
        if(!isset(self::$contractors[$slug]))
            self::$contractors[$slug] = new (__NAMESPACE__.'\\'.Str::studly($slug))();

        return self::$contractors[$slug];
    }

    abstract function getProductList(): array;
    abstract function getProductDetail(string $code): ?array;
    abstract function getAvailableProducts(Carbon $travelDate): void;
    abstract function getProductAvailability(string $code, Carbon $travelDate): bool;
}
