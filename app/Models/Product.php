<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property Collection $availabilities
 * @property string $description
 * @property string $thumbnail
 */
class Product extends Model
{
    use HasFactory;

    public function availabilities()
    {
        return $this->hasMany(Availability::class);
    }

    public function getMinimumPriceAttribute(): ?float
    {
        return $this->availabilities->pluck('price')->min() ?? null;
    }
}
