<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    use HasFactory;

    protected $fillable = [
        'price',
        'start_time',
        'end_time',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Scope a query to only include available entities.
     *
     * @param Builder $query
     * @param Carbon|null $startDate
     * @param Carbon|null $endDate
     * @return Builder
     */
    public function scopeAvailable(Builder $query, Carbon $startDate = null, Carbon $endDate = null): Builder
    {
        $startDate = $startDate ?? now()->startOfDay();
        $endDate = $endDate ?? now()->addWeeks(2)->endOfDay();

        return $query
            ->whereNotNull('price')
            ->whereDate('start_time', '<=', $endDate)
            ->whereDate('end_time', '>=', $startDate);
    }
}
