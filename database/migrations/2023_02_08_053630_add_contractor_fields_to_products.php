<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('code')->after('thumbnail')->default('');
            $table->string('contractor')->after('thumbnail')->default('');
        });
        DB::statement('UPDATE products SET code = id');
        Schema::table('products', function (Blueprint $table) {
            $table->unique(['contractor', 'code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropUnique(['contractor', 'code']);
            $table->dropColumn('contractor');
            $table->dropColumn('code');
        });
    }
};
